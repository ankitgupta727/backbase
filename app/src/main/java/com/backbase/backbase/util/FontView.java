package com.backbase.backbase.util;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

public class FontView extends AppCompatTextView {
  public FontView(Context context) {
    super(context);
    setFont();

  }

  public FontView(Context context, AttributeSet attrs) {
    super(context, attrs);
    setFont();
  }

  public FontView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    setFont();
  }

  private void setFont() {
    Typeface font = Typeface.createFromAsset(getContext().getAssets(),
        "fonts/fontawesome-webfont.ttf");
    setTypeface(font, Typeface.NORMAL);

  }



}
