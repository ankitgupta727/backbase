package com.backbase.backbase.util;

public interface IPredicate<T> {
  boolean apply(T type);
}
