package com.backbase.backbase.util;

import com.backbase.backbase.model.City;
import java.util.Comparator;

public class SortByAlpha implements Comparator<City> {
    @Override
    public int compare(City o1, City o2) {
        int res = String.CASE_INSENSITIVE_ORDER.compare(o1.getName(), o2.getName());
        if (res == 0) {
            res = o1.getName().compareTo(o2.getName());
        }
        return res;
    }
}
