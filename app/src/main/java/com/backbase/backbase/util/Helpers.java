package com.backbase.backbase.util;

import android.content.Context;
import android.content.res.AssetManager;
import android.text.TextUtils;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;

public class Helpers {

    private static final String TAG ="Helpers";

    public static String getDataFromAssets(Context context, String fileName) {

        if (TextUtils.isEmpty(fileName))
            return null;

        if (context != null) {
            try {
                AssetManager manager = context.getAssets();
                InputStream file = manager.open(fileName);
                byte[] formArray = new byte[file.available()];
                file.read(formArray);
                file.close();
                return new String(formArray);
            } catch (IOException ex) {
                Log.e(TAG, ex.getLocalizedMessage(), ex);
            }
        }

        return null;
    }
}
