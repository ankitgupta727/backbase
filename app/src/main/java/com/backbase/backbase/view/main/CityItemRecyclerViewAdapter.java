package com.backbase.backbase.view.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.backbase.backbase.R;
import com.backbase.backbase.model.City;
import com.backbase.backbase.util.FontView;
import com.backbase.backbase.util.IPredicate;
import com.backbase.backbase.util.Predicate;
import com.backbase.backbase.util.SortByAlpha;
import com.backbase.backbase.view.about.AboutActivity;
import com.backbase.backbase.view.main.CityPresenter.FilterLoader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CityItemRecyclerViewAdapter extends RecyclerView.Adapter<CityItemRecyclerViewAdapter.ViewHolder> implements
    Filterable {

  private CityListActivity mParentActivity;
  private List<City> mValues = new ArrayList<>();
  private List<City> mValuesFilter = new ArrayList<>();
  private boolean mTwoPane;
  private FilterLoader filterLoader;
  private View.OnClickListener mOnClickListener = new View.OnClickListener() {
    @Override
    public void onClick(View view) {
      City city = (City) view.getTag();
      switch (view.getId()){
        case R.id.rowContainer:
          if (mTwoPane) {
            Bundle arguments = new Bundle();
            arguments.putInt(MapDetailFragment.ARG_ITEM_ID, city.get_id());
            MapDetailFragment fragment = new MapDetailFragment();
            fragment.setArguments(arguments);
            mParentActivity.getSupportFragmentManager().beginTransaction()
                .replace(R.id.map_detail_container, fragment)
                .commit();
          } else {
            Context context = view.getContext();
            Intent intent = new Intent(context, CityDetailActivity.class);
            intent.putExtra(MapDetailFragment.ARG_ITEM_ID, city.get_id());
            context.startActivity(intent);
          }
          break;
        case R.id.btDetails:
          Context context = view.getContext();
          Intent intent = new Intent(context, AboutActivity.class);
          intent.putExtra(MapDetailFragment.ARG_ITEM_ID, city.get_id());
          context.startActivity(intent);
          break;
      }

    }
  };

  CityItemRecyclerViewAdapter(CityListActivity parent,
      boolean twoPane) {
    mParentActivity = parent;
    mTwoPane = twoPane;
  }

  public void setFilterLoader(FilterLoader filterLoader) {
    this.filterLoader = filterLoader;
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.city_list_content, parent, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    holder.mTvTitle.setText(mValuesFilter.get(position).getName());
    holder.mTvSubTitle.setText( mValuesFilter.get(position).getCountry());
    holder.mTvLatLan.setText(mValuesFilter.get(position).getCoord().getLat() + "," + mValuesFilter.get(position).getCoord().getLat());

    holder.itemView.setTag(mValuesFilter.get(position));
    holder.itemView.setOnClickListener(mOnClickListener);
    holder.btDetails.setTag(mValuesFilter.get(position));
    holder.btDetails.setOnClickListener(mOnClickListener);

  }

  @Override
  public int getItemCount() {
    return mValuesFilter.size();
  }

  @Override
  public Filter getFilter() {
    return new Filter() {
      @Override
      protected FilterResults performFiltering(final CharSequence constraint) {
        final List<City> filteredList = new ArrayList<>();
        if (filterLoader != null) {
          filterLoader.showProgress();
        }
        final String charString = constraint.toString();
        if (charString.isEmpty()) {
          mValuesFilter = mValues;
        } else {
          Predicate.filter(mValues, new IPredicate<City>() {
            @Override
            public boolean apply(City row) {
              if (row.getName().toLowerCase().startsWith(charString.toLowerCase()) || row.getName().startsWith(constraint.toString())) {
                filteredList.add(row);
                Log.d("Search", "Matched With Name" + row.getName());
                return true;
              } else if (row.getCountry().toLowerCase().startsWith(charString.toLowerCase()) || row.getCountry().startsWith(constraint.toString())) {
                filteredList.add(row);
                Log.d("Search", "Matched With Country B:" + "--" + row.getCountry());
                return true;
              }
              return false;
            }
          });
//          for (City row : mValues) {
//            // name match condition. this might differ depending on your requirement
//            // here we are looking for name or phone number match
//            // Log.d("Search","Name and Country:"+row.getName()+"--"+row.getCountry()+"--"+charString);
//            if (row.getName().toLowerCase().startsWith(charString.toLowerCase()) || row.getName().startsWith(constraint.toString())) {
//              filteredList.add(row);
//              Log.d("Search", "Matched With Name" + row.getName());
//            } else if (row.getCountry().toLowerCase().startsWith(charString.toLowerCase()) || row.getCountry().startsWith(constraint.toString())) {
//              filteredList.add(row);
//              Log.d("Search", "Matched With Country B:" + "--" + row.getCountry());
//            }
//          }

          mValuesFilter = filteredList;
        }

        FilterResults filterResults = new FilterResults();
        filterResults.values = mValuesFilter;
        return filterResults;
      }

      @Override
      protected void publishResults(CharSequence constraint, FilterResults results) {
        Collections.sort((List<City>) results.values, new SortByAlpha());
        mValuesFilter = (List<City>) results.values;
        notifyDataSetChanged();
        if (filterLoader != null) {
          filterLoader.hideProgress();
        }
      }
    };
  }

  public void addData(List<City> cities) {
    if (!mValues.isEmpty()) {
      mValues.clear();
    }
    if (!mValuesFilter.isEmpty()) {
      mValuesFilter.clear();
    }
    Collections.sort(cities, new SortByAlpha());
    mValues.addAll(cities);
    mValuesFilter.addAll(cities);
    notifyDataSetChanged();


  }

  class ViewHolder extends RecyclerView.ViewHolder {
    final TextView mTvTitle;
    final TextView mTvSubTitle;
    final TextView mTvLatLan;
    final RelativeLayout btDetails;

    ViewHolder(View view) {
      super(view);
      mTvTitle = (TextView) view.findViewById(R.id.tv_title);
      mTvSubTitle = (TextView) view.findViewById(R.id.tv_sub_title);
      mTvLatLan = (TextView) view.findViewById(R.id.tv_sub_latlan);
      btDetails = (RelativeLayout) view.findViewById(R.id.btDetails);
    }
  }
}
