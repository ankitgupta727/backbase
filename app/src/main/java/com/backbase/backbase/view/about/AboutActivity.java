package com.backbase.backbase.view.about;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.backbase.backbase.App;
import com.backbase.backbase.R;
import com.backbase.backbase.model.City;


public class AboutActivity extends AppCompatActivity implements About.View {

  public static final String ARG_ITEM_ID = "city_id";
  private TextView companyName;
  private TextView companyAddress;
  private TextView companyPostal;
  private TextView companyCity;
  private TextView aboutInfo;
  private ProgressBar progressBar;
  private android.view.View infoContainer;
  private City city;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_about);

    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    toolbar.setTitle(getTitle());
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowHomeEnabled(true);

    companyName = findViewById(R.id.companyName);
    companyAddress = findViewById(R.id.companyAdress);
    companyPostal = findViewById(R.id.companypostal);
    companyCity = findViewById(R.id.companyCity);
    aboutInfo = findViewById(R.id.aboutInfo);
    progressBar = findViewById(R.id.progressBar);
    infoContainer = findViewById(R.id.infoContainer);

    if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(ARG_ITEM_ID)) {
      // Load the dummy content specified by the fragment
      // arguments. In a real-world scenario, use a Loader
      // to load content from a content provider.
      city = App.getInstance().getCity(getIntent().getExtras().getInt(ARG_ITEM_ID));
    }

    AboutPresenterImpl aboutPresenter = new AboutPresenterImpl(this, this);
    aboutPresenter.getAboutInfo();
  }

  @Override
  public void setCompanyName(String companyNameString) {
    infoContainer.setVisibility(android.view.View.VISIBLE);
    companyName.setText(companyNameString);
  }

  @Override
  public void setCompanyAddress(String companyAddressString) {
    companyAddress.setText(companyAddressString);
  }

  @Override
  public void setCompanyPostalCode(String postalCodeString) {
    companyPostal.setText(postalCodeString);
  }

  @Override
  public void setCompanyCity(String companyCityString) {
    if (city != null && !TextUtils.isEmpty(city.getName()) && !TextUtils.isEmpty(city.getCountry())) {
      companyCity.setText(city.getName()+", "+city.getCountry());
    } else {
      companyCity.setText(companyCityString);
    }

  }

  @Override
  public void setAboutInfo(String infoString) {
    aboutInfo.setText(infoString);
  }

  @Override
  public void showError() {
    Toast.makeText(this, getResources().getString(R.string.details_load_error), Toast.LENGTH_LONG).show();

  }

  @Override
  public void showProgress() {
    progressBar.setVisibility(android.view.View.VISIBLE);
  }

  @Override
  public void hideProgress() {
    progressBar.setVisibility(android.view.View.GONE);
  }


  @Override
  public boolean onSupportNavigateUp() {
    onBackPressed();
    return true;
  }
}
