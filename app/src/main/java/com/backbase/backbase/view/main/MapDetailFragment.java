package com.backbase.backbase.view.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.backbase.backbase.App;
import com.backbase.backbase.R;
import com.backbase.backbase.model.City;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * A fragment representing a single Item detail screen. This fragment is either contained in a {@link CityListActivity} in two-pane mode (on tablets)
 * or a {@link CityDetailActivity} on handsets.
 */
public class MapDetailFragment extends Fragment  implements OnMapReadyCallback {

  public static final String ARG_ITEM_ID = "city_id";
  private City city;
  private GoogleMap mMap;

  /**
   * Mandatory empty constructor for the fragment manager to instantiate the fragment (e.g. upon screen orientation changes).
   */
  public MapDetailFragment() {
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    if (getArguments().containsKey(ARG_ITEM_ID)) {
      // Load the dummy content specified by the fragment
      // arguments. In a real-world scenario, use a Loader
      // to load content from a content provider.
      city = App.getInstance().getCity(getArguments().getInt(ARG_ITEM_ID));
    }
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.map_detail, container, false);

    SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
    mapFragment.getMapAsync(this);

    return rootView;
  }

  @Override
  public void onMapReady(GoogleMap googleMap) {
    mMap = googleMap;
    updateMarkerOnMap();
  }

  private void updateMarkerOnMap() {
    if (city != null) {
      // Add a marker to map and move the camera
      LatLng latLng = new LatLng(city.getCoord().getLat(), city.getCoord().getLon());
      mMap.addMarker(new MarkerOptions().position(latLng).title(city.getName()));
      mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12f));
    }
  }
}
