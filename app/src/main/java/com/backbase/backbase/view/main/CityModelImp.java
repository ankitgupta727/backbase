package com.backbase.backbase.view.main;


import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import com.backbase.backbase.model.City;
import com.backbase.backbase.util.Helpers;
import com.google.gson.Gson;
import java.lang.ref.WeakReference;
import java.util.Arrays;

public class CityModelImp implements CityPresenter.Model {


  private static final String TAG = CityModelImp.class.getSimpleName();
  private static final String FILE_NAME = "cities.json";
  private final CityPresenter.Presenter presenter;
  private final WeakReference<Context> context;

  public CityModelImp(@NonNull CityPresenter.Presenter presenter, @NonNull Context context) {
    this.presenter = presenter;
    this.context = new WeakReference<>(context);
  }

  @Override
  public void getCityList() {

    String citiesData = Helpers.getDataFromAssets(context.get(), FILE_NAME);
    if (!TextUtils.isEmpty(citiesData)) {
      City[] cities = new Gson().fromJson(citiesData, City[].class);
      if (cities != null && cities.length > 0) {
        Log.d(TAG, "Cities Size:" + cities.length);
        presenter.onSuccess(Arrays.asList(cities));
        return;
      }
    }

    Log.d(TAG, "Cities Size:0");
    presenter.onFail();
  }
}
