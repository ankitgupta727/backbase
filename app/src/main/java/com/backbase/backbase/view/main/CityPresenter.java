package com.backbase.backbase.view.main;


import com.backbase.backbase.model.City;
import java.util.List;

/**
 * Created by Backbase R&D B.V on 28/06/2018. MVP contract for AboutActivity
 */

public interface CityPresenter {

  interface Model {

    void getCityList();
  }

  interface Presenter {

    void getCityList();

    void onSuccess(List<City> cities);

    void onFail();
  }

  interface View {

    void setCityList(List<City> cities);

    void showError();

    void showProgress();

    void hideProgress();
  }

  interface CityLoader {

    void dataLoaded();

    void dataLoadedFailed();

    void showProgress();

    void hideProgress();

  }

  interface FilterLoader {

    void showProgress();

    void hideProgress();

  }


}

