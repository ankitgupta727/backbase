package com.backbase.backbase.view.main;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.backbase.backbase.App;
import com.backbase.backbase.R;
import com.backbase.backbase.model.City;
import com.backbase.backbase.util.DividerItemDecoration;
import java.util.List;

/**
 * An activity representing a list of Items. This activity has different presentations for handset and tablet-size devices. On handsets, the activity
 * presents a list of items, which when touched, lead to a {@link CityDetailActivity} representing item details. On tablets, the activity presents the
 * list of items and item details side-by-side using two vertical panes.
 */
public class CityListActivity extends AppCompatActivity implements CityPresenter.View, CityPresenter.CityLoader,CityPresenter.FilterLoader{

  /**
   * Whether or not the activity is in two-pane mode, i.e. running on a tablet device.
   */
  private boolean mTwoPane;
  private ProgressDialog progressBar;
  private CityItemRecyclerViewAdapter cityItemRecyclerViewAdapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_city_list);

    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    toolbar.setTitle(getTitle());

    if (findViewById(R.id.map_detail_container) != null) {
      // The detail container view will be present only in the
      // large-screen layouts (res/values-w900dp).
      // If this view is present, then the
      // activity should be in two-pane mode.
      mTwoPane = true;
    }

    View recyclerView = findViewById(R.id.rv_city_list);
    assert recyclerView != null;
    setupRecyclerView((RecyclerView) recyclerView);

    View searchView = findViewById(R.id.rv_search);
    assert searchView != null;
    setupSearchView((SearchView) searchView);

    setProgressBar();


    if(cityItemRecyclerViewAdapter!=null && App.getInstance().getCitiesList().isEmpty()){
      CityPresenterImp cityPresenter = new CityPresenterImp(this, this);
      cityPresenter.getCityList();
    }else{
      if(cityItemRecyclerViewAdapter!=null){
        cityItemRecyclerViewAdapter.addData(App.getInstance().getCitiesList());
      }
    }


  }

  private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
    cityItemRecyclerViewAdapter = new CityItemRecyclerViewAdapter(this, mTwoPane);
    //recyclerView.addItemDecoration(new DividerItemDecoration(this, null));
    recyclerView.setAdapter(cityItemRecyclerViewAdapter);
  }

  private void setupSearchView(@NonNull SearchView searchView) {
    searchView.setActivated(true);
    searchView.setQueryHint("Type your city name here");
    searchView.onActionViewExpanded();
    searchView.setIconified(false);
    searchView.clearFocus();
    searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
      @Override
      public boolean onQueryTextSubmit(String s) {
        return false;
      }

      @Override
      public boolean onQueryTextChange(String s) {
        if (cityItemRecyclerViewAdapter != null) {
          cityItemRecyclerViewAdapter.getFilter().filter(s);
        }
        return false;
      }
    });
  }

  @Override
  public void setCityList(final List<City> cities) {
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        if (cities != null && !cities.isEmpty()) {
          App.getInstance().setCityHashMap(cities);
          if (cityItemRecyclerViewAdapter != null) {
            cityItemRecyclerViewAdapter.addData(cities);
          }
        }
      }
    });

  }

  @Override
  public void showError() {
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        Toast.makeText(CityListActivity.this, getResources().getString(R.string.cities_load_error), Toast.LENGTH_LONG).show();
      }
    });

  }

  @Override
  public void dataLoaded() {
    CityPresenterImp cityPresenterImp = new CityPresenterImp(this, this);
    cityPresenterImp.getCityList();
  }

  @Override
  public void dataLoadedFailed() {
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        Toast.makeText(CityListActivity.this, getResources().getString(R.string.cities_load_error), Toast.LENGTH_LONG).show();
      }
    });
  }

  @Override
  public void showProgress() {
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        if (progressBar != null && !progressBar.isShowing()) {
          progressBar.show();
        }
      }
    });

  }

  @Override
  public void hideProgress() {
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        if (progressBar != null && progressBar.isShowing()) {
          progressBar.dismiss();

        }
      }
    });

  }

  private void setProgressBar(){
    if(progressBar==null){
      progressBar=new ProgressDialog(this);
      progressBar.setMessage(getResources().getString(R.string.progress));
      progressBar.setCanceledOnTouchOutside(false);
      progressBar.setCancelable(false);
    }
  }
}
