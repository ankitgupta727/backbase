package com.backbase.backbase.view.main;

import android.content.Context;
import android.support.annotation.NonNull;
import com.backbase.backbase.model.City;
import com.backbase.backbase.util.AppExecutors;
import java.lang.ref.WeakReference;
import java.util.List;


public class CityPresenterImp implements CityPresenter.Presenter {

    private final WeakReference<CityPresenter.View> cityView;
    private final CityModelImp cityModelImp;

    public CityPresenterImp(CityPresenter.View view, @NonNull Context context){
        this.cityView = new WeakReference<>(view);
        this.cityModelImp = new CityModelImp(this, context);
    }

    @Override
    public void getCityList() {
        CityPresenter.View cityImp = cityView.get();
        if(cityImp != null){
            cityImp.showProgress();
        }
        new AppExecutors().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                cityModelImp.getCityList();
            }
        });
    }

    @Override
    public void onSuccess(List<City> cities) {
        CityPresenter.View cityImp = cityView.get();

        if (cityImp != null) {
            cityImp.setCityList(cities);
            cityImp.hideProgress();
        }
    }

    @Override
    public void onFail() {
        CityPresenter.View cityImp = cityView.get();
        if (cityImp != null){
            cityImp.hideProgress();
            cityImp.showError();
        }
    }
}
