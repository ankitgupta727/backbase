package com.backbase.backbase;

import android.app.Application;
import com.backbase.backbase.model.City;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class App extends Application {
  private static final String TAG = "App";
  // Create the instance
  private static App instance;
  private HashMap<Integer, City> cityHashMap=new HashMap<>();
  private List<City> cities=new ArrayList<>();

  public static App getInstance() {
    if (instance == null) {
      synchronized (App.class) {
        if (instance == null) {
          instance = new App();
        }
      }
    }
    // Return the instance
    return instance;
  }

  @Override
  public void onCreate() {
    super.onCreate();
  }

  public HashMap<Integer, City> getCityHashMap() {
    return cityHashMap;
  }

  public List<City> getCitiesList() {
    return cities;
  }

  public void setCityHashMap(List<City > cityList) {
    if(cityList!=null && !cityList.isEmpty()){
      for(City city:cityList){
        cityHashMap.put(city.get_id(),city);
        cities.add(city);
      }
    }
  }

  public City getCity(int cityId) {

    if (cityHashMap != null && !cityHashMap.isEmpty()) {

      return cityHashMap.get(cityId);
    }
    return null;
  }

}
